<?php

declare(strict_types=1);

require_once __DIR__ . '/../bootstrap.php';

class GameRoundGetterTest extends \Tester\TestCase
{
    public function testGetRound(): void
    {
        $dateTimeFactory = new class extends \App\DateTime\DateTimeFactory {
            public function current(): \DateTime
            {
                return new \DateTime('2017-12-04 13:10:02');
            }
        };
        $getter = new \App\Game\GameRoundGetter($dateTimeFactory);
        \Tester\Assert::same(4, $getter->getCurrentRound());
    }
}

$test = new GameRoundGetterTest();
$test->run();

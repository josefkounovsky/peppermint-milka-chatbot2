<?php

declare(strict_types=1);

require_once __DIR__ . '/../bootstrap.php';

class ReceiptDataValidatorTest extends \Tester\TestCase
{
    private $validator;

    public function __construct()
    {
        $this->validator = new \App\Receipt\ReceiptDataValidator();
    }

    /**
     * @dataProvider dataName
     */
    public function testValidName(string $data, bool $value): void
    {
        \Tester\Assert::same($value, $this->validator->validName($data));
    }

    public function dataName() : array
    {
        return [
            ['John Doe', true],
            ['Václav Makeš', true],
            ['First Second Name', true],
            ['Václav 123', false],
            ['777 666 555', false],
        ];
    }

    /**
     * @dataProvider dataPhone
     */
    public function testValidPhone(string $data, bool $value): void
    {
        \Tester\Assert::same($value, $this->validator->validPhone($data));
    }

    public function dataPhone() : array
    {
        return [
            ['+420666777888', true],
            ['+420 666 777 888', true],
            ['+421 666777888', true],
            ['+420 66 67 77 88 8', true],
            ['777 666 555', false],
        ];
    }
}

$test = new ReceiptDataValidatorTest();
$test->run();

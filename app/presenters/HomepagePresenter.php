<?php

namespace App\Presenters;

use App\Receipt\ReceiptFacade;
use App\Receipt\ReceiptInRoundException;
use App\Receipt\ReceiptRepository;
use App\Receipt\WinnReceiptException;
use Nette\Http\Request;
use Tracy\Debugger;

class HomepagePresenter extends BasePresenter {

    const UPLOAD_DIR = __DIR__ . '/../../www/data/';

    /** @var ReceiptRepository @inject */
    public $receiptRepository;

    /** @var ReceiptFacade @inject */
    public $receiptFacade;

    protected function createComponentCompetition() {

        $form = new \Nette\Application\UI\Form();
        $form->addText('firstname', $this->translator->translate('ui.firstname'))
                ->setAttribute('tabindex', 1)
                ->setAttribute('placeholder', $this->translator->translate('ui.firstname'))
                ->setRequired($this->translator->translate('ui.firstnameRequired'))
                ->addRule(\Nette\Application\UI\Form::PATTERN, $this->translator->translate('ui.notValidFirstname'), '^([a-žA-Ž]{1,} ?[a-Ža-ž]{0,})$');

        $form->addText('lastname', $this->translator->translate('ui.lastname'))
                ->setAttribute('tabindex', 2)
                ->setAttribute('placeholder', $this->translator->translate('ui.lastname') )
                ->setRequired($this->translator->translate('ui.lastnameRequired'))
                ->addRule(\Nette\Application\UI\Form::PATTERN, $this->translator->translate('ui.notValidLastname'), '^([A-Ža-ž]{0,})$');

        $form->addText('adress', $this->translator->translate('ui.adress'))
                ->setAttribute('tabindex', 4)
                ->setAttribute('placeholder',$this->translator->translate('ui.adress'))
                ->setRequired($this->translator->translate('ui.adressRequired'));

        $form->addEmail('email', $this->translator->translate('ui.email'))
                ->setAttribute('tabindex', 3)
                ->setAttribute('placeholder',$this->translator->translate('ui.email'))
                ->setRequired($this->translator->translate('ui.emailRequired'))
                ->addRule(\Nette\Application\UI\Form::EMAIL, $this->translator->translate('ui.notValidEmail'));

        $form->addText('zip', $this->translator->translate('ui.zip'))
                ->setAttribute('tabindex', 5)
                ->setAttribute('placeholder', $this->translator->translate('ui.zip'))
                ->setRequired($this->translator->translate('ui.zipRequired'))
                ->addRule(\Nette\Application\UI\Form::PATTERN, $this->translator->translate('ui.notValidZip'), '\d{3}[ ]?\d{2}|\d{2}[ ]?\d{3}');

        $form->addText('phone', $this->translator->translate('ui.phone'))
                ->setAttribute('tabindex', 6)
                ->setAttribute('placeholder', $this->translator->translate('ui.phone'))
                ->setRequired($this->translator->translate('ui.phoneRequired'))
                ->addRule(\Nette\Application\UI\Form::PATTERN, $this->translator->translate('ui.notValidPhone'), '^(\+420|\+421) ?[1-9][0-9]{2} ?[0-9]{3} ?[0-9]{3}$');
        // musí odpovídat regularnímu výrazu

        $form->addUpload('image', $this->translator->translate('ui.image'))
                ->setRequired($this->translator->translate('ui.imageRequired'))
                ->addRule(\Nette\Application\UI\Form::IMAGE, $this->translator->translate('ui.mustBePhoto'));

        $form->addCheckbox('terms', $this->translator->translate('ui.terms'))
                ->setRequired($this->translator->translate('ui.termsRequired'));

        $form->addText('eet', $this->translator->translate('ui.eet'))
                ->setAttribute('tabindex', 7)
                ->setAttribute('placeholder', $this->translator->translate('ui.eet'))
                ->setRequired($this->translator->translate('ui.eetRequired'));

        $form->addSubmit('send', $this->translator->translate('ui.send'));

//        $form->onValidate[] = [$this, 'formMainValidate'];
        $form->onSuccess[] = [$this, 'formOnSuccess'];

        return $form;
    }

    public function formMainValidate(\Nette\Application\UI\Form $form) {
        $values = $form->getValues();

        $rows = $this->participantsListRepository->findBy([
            'eet' => $values->eet,
            'internal_status IN' => ["win","awaiting_approval", "approved"],
        ]);
        
        if ($rows) {
            foreach($rows as $row) {
                switch ($row->internal_status) {
                    case 'win':
                        $this->redirect('Info:win');//$form['eet']->addError($this->translator->translate('ui.alreadyWin'));
                        break;

                    case 'awaiting_approval':
                        $this->redirect('Info:awaiting');//$form['eet']->addError($this->translator->translate('ui.awaitingApproval'));
                        break;

                    case 'approved':
                        $this->redirect('Info:approved');//$form['eet']->addError($this->translator->translate('ui.approved'));
                        break;

                    default:
                        break;
                }
            }
        }

        if ($form->hasErrors()) {
            $this->redrawControl();
        }
    }

    public function formOnSuccess(\Nette\Application\UI\Form $form) {

        try {
            $values = $form->getValues();
            $file = $values->image;
            if ($file->isImage() and $file->isOk()) {
                $file_ext = strtolower(mb_substr($file->getSanitizedName(), strrpos($file->getSanitizedName(), ".")));
                $file_name = uniqid(rand(0, 20), TRUE) . $file_ext;
                $file->move(self::UPLOAD_DIR . $file_name);

                //miniatura
                $image = \Nette\Utils\Image::fromFile(self::UPLOAD_DIR . $file_name);
                if ($image->getWidth() > $image->getHeight()) {
                    $image->resize(400, NULL);
                } else {
                    $image->resize(NULL, 400);
                }
                $image->sharpen();
                $image->save(self::UPLOAD_DIR . 'thumbs/' . $file_name);
            }
        } catch (\Nette\Neon\Exception $e) {
            $form->addError($e);
        }

        try {
            $this->receiptFacade->createReceipt(
                $this->params['locale'] ?? 'cs_CZ',
                $values->eet,
                $values->firstname . ' ' . $values->lastname,
                $values->adress,
                $values->adress . ' ' . $values->zip,
                $values->phone,
                $file_name,
                null
            );
            $this->redirect('Info:thanks');
        } catch (WinnReceiptException $e) {
            $this->redirect('Info:win');
        } catch (ReceiptInRoundException $e) {
            $this->redirect('Info:awaiting');
        }

        $this->redrawControl();
    }

}

<?php

declare(strict_types=1);

namespace App\Presenters;

use App\Database\Persister;
use App\Notification\Notification;
use App\Receipt\ReceiptRepository;
use Nette;


class AdminPresenter extends Nette\Application\UI\Presenter
{
    private $receiptRepository;
    private $persister;

    public function __construct(
        ReceiptRepository $receiptRepository,
        Persister $persister
    )
    {
        parent::__construct();
        $this->receiptRepository = $receiptRepository;
        $this->persister = $persister;
    }

    protected function startup()
    {
        parent::startup();
        if (!$this->getUser()->isLoggedIn()) {
            $this->redirect('Login:default');
        }
    }

    protected function beforeRender()
    {
        parent::beforeRender();
        $this->setLayout(__DIR__ . '/templates/@adminLayout.latte');


        $this->template->add(
            'countForApprove',
            $this->receiptRepository->findCountForApprove()
        );
    }

    public function renderQueue()
    {
        $this->template->add(
            'receipt',
            $this->receiptRepository->findOneForApprove()
        );
    }

    public function handleApprove(int $receiptId) : void
    {
        $receipt = $this->receiptRepository->getById($receiptId);
        $receipt->approve();
        $this->persister->persist($receipt);

        $this->flashMessage('success', 'Účtenka byla schválena.');
        $this->redirect('queue');
    }

    public function handleDisApprove(int $receiptId) : void
    {
        $receipt = $this->receiptRepository->getById($receiptId);
        $receipt->disApprove();
        $this->persister->persist($receipt);

        $notification = new Notification($receipt);
        $this->persister->persist($notification);

        $this->flashMessage('success', 'Účtenka byla zamítnuta.');
        $this->redirect('queue');
    }

    public function renderStats()
    {
        $this->template->add(
            'countApproved',
            $this->receiptRepository->countApproved()
        );
    }
}

<?php

declare(strict_types=1);

namespace App\Presenters;

use Nette;
use Tracy\Debugger;


class LoginPresenter extends Nette\Application\UI\Presenter
{
    protected function createComponentRegistrationForm()
    {
        $form = new Nette\Application\UI\Form();
        $form->addText('login', 'Jméno:');
        $form->addPassword('password', 'Heslo:');
        $form->addSubmit('send', 'Login');
        $form['send']->getControlPrototype()->class = 'btn-red';
        $form->onSuccess[] = [$this, 'loginFormSucceeded'];

        return $form;
    }

    public function loginFormSucceeded(Nette\Application\UI\Form $form, $values)
    {
        Debugger::barDump($values);
        try {
            $this->user->login($values['login'], $values['password']);
            $this->flashMessage('Byl jste úspěšně přihlášen.');
            $this->redirect('Admin:');
        } catch (Nette\Security\AuthenticationException $e) {
            $this->flashMessage('Zadané přihlašovací údaje jsou neplatné, zkuste to prosím znovu.');
        }

    }
}

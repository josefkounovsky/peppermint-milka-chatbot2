<?php

namespace App\Presenters;

use App\Game\GameRoundGetter;
use App\Receipt\ReceiptRepository;

class WinnersPresenter extends BasePresenter
{
    /** @var GameRoundGetter @inject */
    public $gameRoundGetter;

    /** @var ReceiptRepository @inject */
    public $receiptRepository;

    public function renderDefault($id = null)
    {
        if ($id === null || $id <= 0 || $id > 31) {
            $round = $this->gameRoundGetter->getCurrentRound();
        } else {
            $round = (int) $id;
        }

        $this->template->add('round', $round);
        $this->template->add('winners', $this->receiptRepository->findWinners($round));
    }
}

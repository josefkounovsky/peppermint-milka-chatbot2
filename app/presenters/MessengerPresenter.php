<?php

declare(strict_types=1);

namespace App\Presenters;

use App\ChatBot\Bot;
use BotMan\BotMan\BotMan;
use Nette;


class MessengerPresenter extends Nette\Application\UI\Presenter
{
    private $bot;
    private $botMan;

    public function __construct(
        Bot $bot,
        BotMan $botMan
    )
    {
        parent::__construct();
        $this->bot = $bot;
        $this->botMan = $botMan;
    }

    public function renderDefault()
    {
        $this->botMan->fallback(function (BotMan $botMan) {
            $this->bot->run($botMan);
        });
        $this->botMan->listen();

        die;
    }

    private function verifyFacebookWebhook()
    {
        $this->sendJson((int) $_GET['hub_challenge']);
    }
}

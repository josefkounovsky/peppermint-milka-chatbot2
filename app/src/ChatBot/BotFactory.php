<?php

declare(strict_types=1);

namespace App\ChatBot;

use BotMan\BotMan\BotMan;
use BotMan\BotMan\BotManFactory;
use BotMan\BotMan\Drivers\DriverManager;
use BotMan\Drivers\Facebook\FacebookDriver;
use BotMan\Drivers\Facebook\FacebookFileDriver;
use BotMan\Drivers\Facebook\FacebookImageDriver;

class BotFactory
{
    public static function create(
        string $appSecret,
        string $pageAccessToken,
        string $verifyToken
    ) : BotMan
    {
        self::loadDrivers();

        $config = [
            'facebook' => [
                'token' => $pageAccessToken,
                'app_secret' => $appSecret,
                'verification'=> $verifyToken,
            ],
        ];
        $botMan = BotManFactory::create($config);

        return $botMan;
    }

    private static function loadDrivers() : void
    {
        $drivers = [
            FacebookImageDriver::class,
            FacebookFileDriver::class,
            FacebookDriver::class,
        ];

        foreach ($drivers as $driver) {
            DriverManager::loadDriver($driver);
        }
    }
}

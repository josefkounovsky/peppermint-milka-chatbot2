<?php

declare(strict_types=1);

namespace App\ChatBot;

use App\Database\Persister;
use App\Receipt\ReceiptDataValidator;
use App\Receipt\ReceiptFacade;
use App\Receipt\ReceiptInRoundException;
use App\Receipt\ReceiptMessenger;
use App\Receipt\ReceiptMessengerRepository;
use App\Receipt\WinnReceiptException;
use BotMan\BotMan\BotMan;
use BotMan\BotMan\Messages\Attachments\Image;
use BotMan\Drivers\Facebook\Extensions\ButtonTemplate;
use BotMan\Drivers\Facebook\Extensions\ElementButton;
use Kdyby\Translation\Translator;
use Nette\Application\LinkGenerator;

class Bot
{
    private $receiptMessengerRepository;
    private $translator;
    private $persister;
    private $validator;
    private $receiptFacade;
    private $linkGenerator;

    public function __construct(
        ReceiptMessengerRepository $receiptMessengerRepository,
        Translator $translator,
        Persister $persister,
        ReceiptDataValidator $validator,
        ReceiptFacade $receiptFacade,
        LinkGenerator $linkGenerator
    )
    {
        $this->receiptMessengerRepository = $receiptMessengerRepository;
        $this->translator = $translator;
        $this->persister = $persister;
        $this->validator = $validator;
        $this->receiptFacade = $receiptFacade;
        $this->linkGenerator = $linkGenerator;
    }

    public function run(BotMan $botman) : void
    {
        $fbId = $botman->getUser()->getId();
        $locale = $botman->getUser()->getInfo()['locale'];
        $fbName = $botman->getUser()->getFirstName() . ' ' . $botman->getUser()->getLastName();
        $receiptMessenger = $this->receiptMessengerRepository->findByFacebookId($fbId);
        if ($receiptMessenger === null) {
            $buttonAgree = $this->translator->trans('button1', [], null, $locale);
            $buttonRepair = $this->translator->trans('button2', [], null, $locale);
            $botman->reply(
                ButtonTemplate::create($this->translator->trans('messenger1', [], null, $locale))
                    ->addButton(
                        ElementButton::create($buttonAgree)
                            ->type(ElementButton::TYPE_POSTBACK)
                            ->payload($buttonAgree)
                    )
                    ->addButton(
                        ElementButton::create($buttonRepair)
                            ->type(ElementButton::TYPE_POSTBACK)
                            ->payload($buttonRepair)
                    )
            );
            $receiptMessenger = new ReceiptMessenger($fbId, $locale);
            $this->persister->persist($receiptMessenger);
            return;
        }

        switch ($receiptMessenger->getStatus()) {
            case ReceiptMessenger::STATUS_INIT:
            case ReceiptMessenger::STATUS_CHAT:
                $keywordForSwitchToChatBot = [
                    $this->translator->trans('button1', [], null, $locale),
                    'Soutěž',
                    'Soutez',
                    'soutez',
                    'soutěž',
                    'Soutěžit',
                    'Soutezit',
                    'soutěžit',
                    'soutezit',
                ];
                $currentMessage = $botman->getMessage()->getText();
                if (in_array($currentMessage, $keywordForSwitchToChatBot, true)) {
                    $message = $this->translator->trans('messenger4', ['%fbname%' => $fbName], null, $locale);
                    $botman->reply($message);
                    $receiptMessenger->nameWasDisplyed();
                } else if ($receiptMessenger->getStatus() === ReceiptMessenger::STATUS_INIT) {
                    $message = $this->translator->trans('messenger2', [], null, $locale);
                    $botman->reply($message);
                    $receiptMessenger->chatWithOperator();
                }
                break;

            case ReceiptMessenger::STATUS_START:
                $name = $botman->getMessage()->getText();
                if ($this->validator->validName($name)) {
                    $receiptMessenger->setName($name);
                    $message = $this->translator->trans('messenger5', ['%databaseName%' => $receiptMessenger->getName()], null, $locale);
                } else {
                    $message = $this->translator->trans('valid1', [], null, $locale);
                }
                $botman->reply($message);
                break;

            case ReceiptMessenger::STATUS_SET_NAME:
                $email = $botman->getMessage()->getText();
                if ($this->validator->validEmail($email)) {
                    $receiptMessenger->setEmail($email);
                    $message = $this->translator->trans('messenger7', [], null, $locale);
                } else {
                    $message = $this->translator->trans('valid2', [], null, $locale);
                }
                $botman->reply($message);
                break;

            case ReceiptMessenger::STATUS_SET_EMAIL:
                $address = $botman->getMessage()->getText();
                $receiptMessenger->setAddress($address);
                $message = $this->translator->trans('messenger8', [], null, $locale);
                $botman->reply($message);
                break;

            case ReceiptMessenger::STATUS_SET_ADDRESS:
                $phone = $botman->getMessage()->getText();
                if ($this->validator->validPhone($phone)) {
                    $receiptMessenger->setPhone($phone);
                    $message = $this->translator->trans('messenger9', [], null, $locale);
                } else {
                    $message = $this->translator->trans('valid3', [], null, $locale);
                }
                $botman->reply($message);
                break;

            case ReceiptMessenger::STATUS_SET_PHONE:
                $images = $botman->getMessage()->getImages();
                if (count($images) === 0) {
                    $message = $this->translator->trans('valid5', [], null, $locale);
                    $botman->reply($message);
                } else {
                    /** @var Image $image */
                    $image = $images[0];
                    $receiptMessenger->setImage($image->getUrl());
                    $message = $this->translator->trans('messenger10', [], null, $locale);
                    $botman->reply($message);
                }
                break;

            case ReceiptMessenger::STATUS_SET_IMAGE:
                $bkp = $botman->getMessage()->getText();
                $receiptMessenger->setBkp($bkp);
                $message = $this->translator->trans('messenger11', [], null, $locale) . "\n";
                $message .= "- Jméno: " . $receiptMessenger->getName() . "\n";
                $message .= "- Email: " . $receiptMessenger->getEmail() . "\n";
                $message .= "- Telefon: " . $receiptMessenger->getPhone() . "\n";
                $message .= "- Adresa: " . $receiptMessenger->getAddress() . "\n";
                $message .= "- Účtenka: " . $receiptMessenger->getImage() . "\n";
                $message .= "- EET: " . $receiptMessenger->getBkp() . "\n";
                $buttonAgree = $this->translator->trans('buttonCorrect', [], null, $locale);
                $buttonRepair = $this->translator->trans('buttonRepair', [], null, $locale);
                $botman->reply(
                    ButtonTemplate::create($message)
                        ->addButton($this->createButton($buttonAgree))
                        ->addButton($this->createButton($buttonRepair))
                );
                break;

            case ReceiptMessenger::STATUS_SET_BKP:
                $buttonAgree = $this->translator->trans('buttonCorrect', [], null, $locale);
                $currentMessage = $botman->getMessage()->getText();
                if ($currentMessage === $buttonAgree) {
                    $receiptMessenger->approve();
                    $message = $this->translator->trans('messenger12', ['%link%' => $this->linkGenerator->link('Info:terms')], null, $locale);
                    $buttonAggree = $this->translator->trans('buttonAggree', [], null, $locale);
                    $buttonDisAggree = $this->translator->trans('buttonDisAggree', [], null, $locale);
                    $botman->reply(
                        ButtonTemplate::create($message)
                            ->addButton($this->createButton($buttonAggree))
                            ->addButton($this->createButton($buttonDisAggree))
                    );
                } else {
                    $receiptMessenger->disApprove();
                    $message = $this->translator->trans('messenger4', ['%fbname%' => $fbName], null, $locale);
                    $botman->reply($message);
                }
                break;

            case ReceiptMessenger::STATUS_SET_APPROVED_INPUT:
                $buttonAgree = $this->translator->trans('buttonAggree', [], null, $locale);
                $currentMessage = $botman->getMessage()->getText();
                if ($currentMessage === $buttonAgree) {
                    $receiptMessenger->agreeWithTerm();
                    try {
                        $this->receiptFacade->createReceipt(
                            $receiptMessenger->getLocale(),
                            $receiptMessenger->getBkp(),
                            $receiptMessenger->getName(),
                            $receiptMessenger->getEmail(),
                            $receiptMessenger->getAddress(),
                            $receiptMessenger->getPhone(),
                            $receiptMessenger->getImage(),
                            $receiptMessenger->getFacebookId()
                        );
                        $message = $this->translator->trans('messenger15', [], null, $locale);
                    } catch (WinnReceiptException $e) {
                        $message = $this->translator->trans('messenger13', [], null, $locale);
                    } catch (ReceiptInRoundException $e) {
                        $message = $this->translator->trans('messenger14', [], null, $locale);
                    }
                    $botman->reply($message);
                    $receiptMessenger->markAsSent();
                }
                break;

            case ReceiptMessenger::STATUS_SENT:
                $button1 = $this->translator->trans('button1', [], null, $locale);
                $button2 = $this->translator->trans('button2', [], null, $locale);
                $message = $this->translator->trans('messenger3', ['%fbname%' => $fbName], null, $locale);
                $botman->reply(
                    ButtonTemplate::create($message)
                        ->addButton($this->createButton($button1))
                        ->addButton($this->createButton($button2))
                );
                $receiptMessenger->showAfterReturn();
                break;

            case ReceiptMessenger::STATUS_BACK_SHOW:
                $keywordForSwitchToChatBot = [
                    $this->translator->trans('button1', [], null, $locale),
                    'Soutez',
                    'soutez',
                    'soutěž',
                    'Soutěžit',
                    'Soutezit',
                    'soutěžit',
                    'soutezit',
                ];
                $currentMessage = $botman->getMessage()->getText();
                if (in_array($currentMessage, $keywordForSwitchToChatBot, true)) {
                    $message = $this->translator->trans('messenger11', [], null, $locale) . "\n";
                    $message .= "- Jméno: " . $receiptMessenger->getName() . "\n";
                    $message .= "- Email: " . $receiptMessenger->getEmail() . "\n";
                    $message .= "- Telefon: " . $receiptMessenger->getPhone() . "\n";
                    $message .= "- Adresa: " . $receiptMessenger->getAddress() . "\n";
                    $buttonAgree = $this->translator->trans('buttonCorrect', [], null, $locale);
                    $buttonRepair = $this->translator->trans('buttonRepair', [], null, $locale);
                    $botman->reply(
                        ButtonTemplate::create($message)
                            ->addButton($this->createButton($buttonAgree))
                            ->addButton($this->createButton($buttonRepair))
                    );
                    $receiptMessenger->showCheckAfterReturn();
                } else {
                    $message = $this->translator->trans('messenger2', [], null, $locale);
                    $botman->reply($message);
                }
                break;

            case ReceiptMessenger::STATUS_CHECK_SHOW:
                $buttonAgree = $this->translator->trans('buttonCorrect', [], null, $locale);
                $currentMessage = $botman->getMessage()->getText();
                if ($currentMessage === $buttonAgree) {
                    $receiptMessenger->setPhone($receiptMessenger->getPhone());
                    $message = $this->translator->trans('messenger9', [], null, $locale);
                } else {
                    $receiptMessenger->disApprove();
                    $message = $this->translator->trans('messenger4', ['%fbname%' => $fbName], null, $locale);
                }
                $botman->reply($message);
                break;
        }

        $this->persister->persist($receiptMessenger);
    }

    private function createButton(string $buttonText) : ElementButton
    {
        return ElementButton::create($buttonText)
            ->type(ElementButton::TYPE_POSTBACK)
            ->payload($buttonText);
    }
}

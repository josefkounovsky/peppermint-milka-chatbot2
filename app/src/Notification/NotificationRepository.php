<?php

declare(strict_types=1);

namespace App\Notification;

use Kdyby\Doctrine\EntityDao;

class NotificationRepository
{
    private $dao;

    public function __construct(EntityDao $dao)
    {
        $this->dao = $dao;
    }

    public function findNextForSend() : ?Notification
    {
        $notification = $this->dao->findOneBy([
            'status' => Notification::STATUS_WAITING_FOR_SENT,
        ]);

        return $notification;
    }
}

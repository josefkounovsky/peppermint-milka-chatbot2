<?php

declare(strict_types=1);

namespace App\Notification;

use App\Receipt\Receipt;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Notification
{
    public const STATUS_WAITING_FOR_SENT = 1;
    public const STATUS_SENT = 2;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="\App\Receipt\Receipt")
     */
    private $receipt;

    /**
     * @ORM\Column(type="smallint")
     */
    private $status;

    public function __construct(
        Receipt $receipt
    )
    {
        $this->receipt = $receipt;
        $this->status = self::STATUS_WAITING_FOR_SENT;
    }

    public function markAsSent() : void
    {
        if ($this->status !== self::STATUS_WAITING_FOR_SENT) {
            throw new \LogicException();
        }
        $this->status = self::STATUS_SENT;
    }

    public function getReceipt() : Receipt
    {
        return $this->receipt;
    }
}

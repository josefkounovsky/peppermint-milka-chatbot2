<?php

declare(strict_types=1);

namespace App\Notification;

use App\Database\Persister;
use App\Receipt\Receipt;
use BotMan\BotMan\BotMan;
use BotMan\Drivers\Facebook\FacebookDriver;
use SendGrid\Content;
use SendGrid\Email;
use SendGrid\Mail;

class NotificationFacade
{
    private $sendGrid;
    private $repository;
    private $botMan;
    private $persister;

    public function __construct(
        \SendGrid $sendGrid,
        NotificationRepository $repository,
        BotMan $botMan,
        Persister $persister
    )
    {
        $this->sendGrid = $sendGrid;
        $this->repository = $repository;
        $this->botMan = $botMan;
        $this->persister = $persister;
    }

    public function notify() : void
    {
        while (true) {
            $notification = $this->repository->findNextForSend();
            if ($notification === null) {
                break;
            }

            if ($notification->getReceipt()->isFromFacebook()) {
                $this->notifyFacebook($notification->getReceipt());
            } else {
                $this->notifyEmail($notification->getReceipt());
            }

            $notification->markAsSent();
            $this->persister->persist($notification);
        }
    }

    private function notifyFacebook(Receipt $receipt) : void
    {
        // todo - ROZMYSLET a dát sem další textace
        if ($receipt->isWinReceipt()) {
            $message = 'Vyhrál jsi! Moc gratulujeme k výhře, tu Ti pošleme na vyplněnou adresu.';
        } else {
            $message = 'Bohužel jsi prohrál, ale nevzdávej to a zkus to příště znovu.';
        }
        $this->botMan->say($message, $receipt->getFacebookId(), FacebookDriver::class);
    }

    private function notifyEmail(Receipt $receipt) : void
    {
        // todo - SEM doplnit konkretni vyherni textace pro emaily
        $from = new Email("Example User", "vaclav@makes.cz");
        $subject = "Milka results";
        $to = new Email($receipt->getName(), $receipt->getEmail());
        $content = new Content(
            "text/plain",
            "You " . ($receipt->isWinReceipt() ? 'win' : 'lose')
        );
        $mail = new Mail($from, $subject, $to, $content);

        $this->sendGrid->client->mail()->send()->post($mail);
    }
}

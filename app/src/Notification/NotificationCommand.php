<?php

declare(strict_types=1);

namespace App\Notification;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class NotificationCommand extends Command
{
    private $facade;

    public function __construct(NotificationFacade $facade)
    {
        parent::__construct();
        $this->facade = $facade;
    }

    protected function configure(): void
    {
        $this->setName('game:notify');
        $this->setDescription('Notify winners and losers');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->facade->notify();

        return 0;
    }
}

<?php

declare(strict_types=1);

namespace App\Dev;

use App\Database\Persister;
use App\Receipt\Receipt;
use Nette\Utils\Random;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RandomReceiptGenerateCommand extends Command
{
    private $persister;

    public function __construct(Persister $persister)
    {
        parent::__construct();
        $this->persister = $persister;
    }

    protected function configure(): void
    {
        $this->setName('dev:generate-receipts');
        $this->setDescription('Generate 100 random receipts');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        for ($i = 0; $i < 100; $i++) {
            $bkp = Random::generate(8) . '-' . Random::generate(8);
            $receipt = new Receipt(
                'cs_CZ',
                $bkp,
                11,
                'John Doe',
                'john@doe.cz',
                'New York 123',
                '+420123123123',
                'image.jpg',
                null
            );
            $this->persister->persist($receipt);
        }

        return 0;
    }
}
<?php

declare(strict_types=1);

namespace App\Database;

use Kdyby\Doctrine\EntityManager;

class Persister
{
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function persist($entity) : void
    {
        $this->entityManager->persist($entity);
        $this->entityManager->flush($entity);
    }
}

<?php

declare(strict_types=1);

namespace App\Game;

use App\DateTime\DateTimeFactory;

class GameRoundGetter
{
    private $dateTimeFactory;

    public function __construct(DateTimeFactory $dateTimeFactory)
    {
        $this->dateTimeFactory = $dateTimeFactory;
    }

    public function getCurrentRound() : int
    {
        $currentDate = $this->dateTimeFactory->current();
        $day = (int) $currentDate->format('j');

        return $day;
    }
}

<?php

declare(strict_types=1);

namespace App\Game;

use App\Database\Persister;
use App\Notification\Notification;
use App\Receipt\Receipt;
use App\Receipt\ReceiptRepository;

class GameFacade
{
    private $persister;
    private $receiptRepository;
    private $gameRoundGetter;

    public function __construct(
        Persister $persister,
        ReceiptRepository $receiptRepository,
        GameRoundGetter $gameRoundGetter
    )
    {
        $this->persister = $persister;
        $this->receiptRepository = $receiptRepository;
        $this->gameRoundGetter = $gameRoundGetter;
    }

    public function runGame() : void
    {
        $gameRound = $this->gameRoundGetter->getCurrentRound() - 1;
        $isCalledInFirstDayOfGame = $gameRound <= 0;
        if ($isCalledInFirstDayOfGame) {
            return;
        }
        $this->selectAndMarkWinners($gameRound);
        $this->selectAndMarkLosers($gameRound);
    }

    private function selectAndMarkWinners(int $round) : void
    {
        $winners = $this->receiptRepository->findRandomWinners($round);
        foreach ($winners as $winner) {
            $winner->win();
            $this->persister->persist($winner);
            $this->enqueueNotification($winner);
        }
    }

    private function selectAndMarkLosers(int $round) : void
    {
        $losers = $this->receiptRepository->findLosers($round);
        foreach ($losers as $loser) {
            $loser->loss();
            $this->persister->persist($loser);
            $this->enqueueNotification($loser);
        }
    }

    private function enqueueNotification(Receipt $receipt) : void
    {
        $notification = new Notification($receipt);
        $this->persister->persist($notification);
    }
}

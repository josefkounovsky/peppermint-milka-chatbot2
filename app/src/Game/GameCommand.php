<?php

declare(strict_types=1);

namespace App\Game;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GameCommand extends Command
{
    private $facade;

    public function __construct(GameFacade $facade)
    {
        parent::__construct();
        $this->facade = $facade;
    }

    protected function configure(): void
    {
        $this->setName('game:run');
        $this->setDescription('Run game and select winners');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->facade->runGame();

        return 0;
    }
}

<?php

declare(strict_types=1);

namespace App\Receipt;

class ReceiptInRoundException extends \Exception
{
}

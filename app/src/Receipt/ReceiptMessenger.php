<?php

declare(strict_types=1);

namespace App\Receipt;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class ReceiptMessenger
{
    public const STATUS_INIT = 0;
    public const STATUS_START = 1;
    public const STATUS_SET_NAME = 2;
    public const STATUS_SET_EMAIL = 3;
    public const STATUS_SET_ADDRESS = 4;
    public const STATUS_SET_PHONE = 5;
    public const STATUS_SET_IMAGE = 6;
    public const STATUS_SET_BKP = 7;
    public const STATUS_SET_APPROVED_INPUT = 8;
    public const STATUS_SET_AGREE_TERM = 9;
    public const STATUS_SENT = 10;
    public const STATUS_BACK_SHOW = 11;
    public const STATUS_CHECK_SHOW = 12;
    public const STATUS_CHAT = 13;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @ORM\Column(type="string", unique=true)
     */
    private $facebookId;

    /**
     * @ORM\Column(type="smallint")
     */
    private $status;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $address;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $image;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $bkp;

    /**
     * @ORM\Column(type="string")
     */
    private $locale;

    public function __construct(
        string $facebookId,
        string $locale
    )
    {
        $this->facebookId = $facebookId;
        $this->locale = $locale;
        $this->status = self::STATUS_INIT;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function getStatus() : int
    {
        return $this->status;
    }

    public function nameWasDisplyed() : void
    {
        if ($this->status !== self::STATUS_INIT && $this->status !== self::STATUS_CHAT) {
            throw new \LogicException('');
        }
        $this->status = self::STATUS_START;
    }

    public function chatWithOperator() : void
    {
        if ($this->status !== self::STATUS_INIT) {
            throw new \LogicException('');
        }
        $this->status = self::STATUS_CHAT;
    }

    public function setName(string $name): void
    {
        if ($this->status !== self::STATUS_START) {
            throw new \LogicException('');
        }
        $this->name = $name;
        $this->status = self::STATUS_SET_NAME;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function setEmail(string $email): void
    {
        if ($this->status !== self::STATUS_SET_NAME) {
            throw new \LogicException('');
        }
        $this->email = $email;
        $this->status = self::STATUS_SET_EMAIL;
    }

    public function setAddress(string $address): void
    {
        if ($this->status !== self::STATUS_SET_EMAIL) {
            throw new \LogicException('');
        }
        $this->address = $address;
        $this->status = self::STATUS_SET_ADDRESS;
    }

    public function setPhone(string $phone): void
    {
        if ($this->status !== self::STATUS_SET_ADDRESS && $this->status !== self::STATUS_CHECK_SHOW) {
            throw new \LogicException('');
        }
        $this->phone = $phone;
        $this->status = self::STATUS_SET_PHONE;
    }

    public function setImage(string $image): void
    {
        if ($this->status !== self::STATUS_SET_PHONE) {
            throw new \LogicException('');
        }
        $this->image = $image;
        $this->status = self::STATUS_SET_IMAGE;
    }

    public function setBkp(string $bkp): void
    {
        if ($this->status !== self::STATUS_SET_IMAGE) {
            throw new \LogicException('');
        }
        $this->bkp = $bkp;
        $this->status = self::STATUS_SET_BKP;
    }

    public function approve() : void
    {
        $this->status = self::STATUS_SET_APPROVED_INPUT;
    }

    public function disApprove() : void
    {
        $this->status = self::STATUS_START;
    }

    public function agreeWithTerm() : void
    {
        $this->status = self::STATUS_SET_AGREE_TERM;
    }

    public function getEmail() : string
    {
        return $this->email;
    }

    public function getAddress() : string
    {
        return $this->address;
    }

    public function getPhone() : string
    {
        return $this->phone;
    }

    public function getImage() : string
    {
        return $this->image;
    }

    public function getBkp() : string
    {
        return $this->bkp;
    }

    public function getFacebookId() : string
    {
        return $this->facebookId;
    }

    public function markAsSent() : void
    {
        if ($this->status !== self::STATUS_SET_AGREE_TERM) {
            throw new \LogicException('');
        }
        $this->status = self::STATUS_SENT;
    }

    public function showAfterReturn() : void
    {
        if ($this->status !== self::STATUS_SENT) {
            throw new \LogicException();
        }
        $this->status = self::STATUS_BACK_SHOW;
    }

    public function showCheckAfterReturn() : void
    {
        if ($this->status !== self::STATUS_BACK_SHOW) {
            throw new \LogicException();
        }
        $this->status = self::STATUS_CHECK_SHOW;
    }

    public function getLocale() : string
    {
        return $this->locale;
    }
}

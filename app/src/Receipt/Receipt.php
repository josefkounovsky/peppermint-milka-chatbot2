<?php

declare(strict_types=1);

namespace App\Receipt;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(
 *     indexes={
 *         @ORM\Index(name="idx_round", columns={"round"}),
 *     }
 * )
 */
class Receipt
{
    public const STATUS_AWAITING_APPROVAL = 1;
    public const STATUS_INVALID = 2;
    public const STATUS_APPROVED = 3;
    public const STATUS_WIN = 4;
    public const STATUS_LOSS = 5;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $bkp;

    /**
     * @ORM\Column(type="smallint")
     */
    private $status;

    /**
     * @ORM\Column(type="integer")
     */
    private $round;

    /**
     * @ORM\Column(type="string")
     */
    private $locale;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="string")
     */
    private $email;

    /**
     * @ORM\Column(type="string")
     */
    private $address;

    /**
     * @ORM\Column(type="string")
     */
    private $phone;

    /**
     * @ORM\Column(type="string")
     */
    private $image;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $facebookId;

    public function __construct(
        string $locale,
        string $bkp,
        int $round,
        string $name,
        string $email,
        string $address,
        string $phone,
        string $image,
        string $facebookId = null
    )
    {
        $this->locale = $locale;
        $this->bkp = $bkp;
        $this->status = self::STATUS_AWAITING_APPROVAL;
        $this->round = $round;
        $this->name = $name;
        $this->email = $email;
        $this->address = $address;
        $this->phone = $phone;
        $this->image = $image;
        $this->facebookId = $facebookId;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function getBkp() : string
    {
        return $this->bkp;
    }

    public function disApprove() : void
    {
        if ($this->status !== self::STATUS_AWAITING_APPROVAL) {
            throw new \LogicException('Impossible change status');
        }
        $this->status = self::STATUS_INVALID;
    }

    public function approve() : void
    {
        if ($this->status !== self::STATUS_AWAITING_APPROVAL) {
            throw new \LogicException('Impossible change status');
        }
        $this->status = self::STATUS_APPROVED;
    }

    public function win() : void
    {
        if ($this->status !== self::STATUS_APPROVED) {
            throw new \LogicException('Impossible change status');
        }
        $this->status = self::STATUS_WIN;
    }

    public function loss() : void
    {
        if ($this->status !== self::STATUS_APPROVED) {
            throw new \LogicException('Impossible change status');
        }
        $this->status = self::STATUS_LOSS;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function getAddress() : string
    {
        return $this->address;
    }

    public function getImage() : string
    {
        return $this->image;
    }

    public function isFromFacebook() : bool
    {
        return $this->facebookId !== null;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function isWinReceipt() : bool
    {
        return $this->status === self::STATUS_WIN;
    }

    public function getFacebookId() : ?string
    {
        return $this->facebookId;
    }
}

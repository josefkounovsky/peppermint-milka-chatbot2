<?php

declare(strict_types=1);

namespace App\Receipt;

use Kdyby\Doctrine\EntityDao;

class ReceiptMessengerRepository
{
    private $dao;

    public function __construct(EntityDao $dao)
    {
        $this->dao = $dao;
    }

    public function findByFacebookId(string $facebookId): ?ReceiptMessenger
    {
        $receiptMessenger = $this->dao->findOneBy([
            'facebookId' => $facebookId,
        ]);

        return $receiptMessenger;
    }
}

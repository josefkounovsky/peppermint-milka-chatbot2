<?php

declare(strict_types=1);

namespace App\Receipt;

use App\Database\Persister;
use App\Game\GameRoundGetter;

class ReceiptFacade
{
    private $persister;
    private $receiptRepository;
    private $gameRoundGetter;

    public function __construct(
        Persister $persister,
        ReceiptRepository $receiptRepository,
        GameRoundGetter $gameRoundGetter
    )
    {
        $this->persister = $persister;
        $this->receiptRepository = $receiptRepository;
        $this->gameRoundGetter = $gameRoundGetter;
    }

    public function createReceipt(
        string $locale,
        string $bkp,
        string $name,
        string $email,
        string $address,
        string $phone,
        string $image,
        string $facebookId = null
    ) : void
    {
        if ($this->receiptRepository->existsWinnerByBkp($bkp)) {
            throw new WinnReceiptException();
        }

        $currentRound = $this->gameRoundGetter->getCurrentRound();
        if ($this->receiptRepository->existsReceiptByBkpAndRound($bkp, $currentRound)) {
            throw new ReceiptInRoundException();
        }

        $receipt = new Receipt(
            $locale,
            $bkp,
            $currentRound,
            $name,
            $email,
            $address,
            $phone,
            $image,
            $facebookId
        );
        $this->persister->persist($receipt);
    }
}

<?php

declare(strict_types=1);

namespace App\Receipt;

use Nette\Utils\Strings;
use Nette\Utils\Validators;

class ReceiptDataValidator
{
    public function validName(string $name) : bool
    {
        $name = Strings::lower($name);
        if (!preg_match('~^[a-zěščřžýáíéúůó ]+$~', $name)) {
            return false;
        }
        return true;
    }

    public function validEmail(string $email) : bool
    {
        return Validators::isEmail($email);
    }

    public function validPhone(string $phone) : bool
    {
        $phone = str_replace(' ', '', $phone);
        if (!preg_match('~^\+42(0|1)[0-9]{9}$~', $phone)) {
            return false;
        }
        return true;
    }
}

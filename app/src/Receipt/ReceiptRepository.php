<?php

declare(strict_types=1);

namespace App\Receipt;

use Doctrine\DBAL\Types\IntegerType;
use Doctrine\DBAL\Types\Type;
use Kdyby\Doctrine\EntityDao;

class ReceiptRepository
{
    private const COUNT_WINNERS = 333;

    private $dao;

    public function __construct(EntityDao $dao)
    {
        $this->dao = $dao;
    }

    public function findOneForApprove() : ?Receipt
    {
        $receipt = $this->dao->findOneBy([
            'status' => Receipt::STATUS_AWAITING_APPROVAL,
        ]);

        return $receipt;
    }

    public function findCountForApprove() : int
    {
        $receipt = $this->dao->countBy([
            'status' => Receipt::STATUS_AWAITING_APPROVAL,
        ]);

        return $receipt;
    }

    public function countApproved() : int
    {
        $count = $this->dao->countBy([
            'status' => Receipt::STATUS_APPROVED,
        ]);

        return $count;
    }

    public function getById(int $receiptId) : Receipt
    {
        return $this->dao->find($receiptId);
    }

    /**
     * @return Receipt[]
     */
    public function findRandomWinners(int $round) : array
    {
        $randomWinnerIdsQuery = '
            SELECT receipt.id
            FROM receipt
            WHERE
                receipt.status = :status
                AND receipt.round = :round
            ORDER BY RAND()
            LIMIT :limit
        ';
        $connection = $this->dao->getEntityManager()->getConnection();
        $stmt = $connection->prepare($randomWinnerIdsQuery);
        $stmt->bindValue('status', Receipt::STATUS_APPROVED, Type::INTEGER);
        $stmt->bindValue('round', $round, Type::INTEGER);
        $stmt->bindValue('limit', self::COUNT_WINNERS, Type::INTEGER);
        $stmt->execute();
        $winnersId = $stmt->fetchAll(\PDO::FETCH_COLUMN);
        $winnersId = array_map('intval', $winnersId);

        $qb = $this->dao->createQueryBuilder('receipt')
            ->where('receipt.id IN (:ids)')
            ->setParameter('ids', $winnersId);

        return $qb->getQuery()->getResult();
    }

    /**
     * @return Receipt[]
     */
    public function findLosers(int $round) : array
    {
        return $this->dao->findBy([
            'status' => Receipt::STATUS_APPROVED,
            'round' => $round,
        ]);
    }

    /**
     * @return Receipt[]
     */
    public function findWinners(int $round) : array
    {
        return $this->dao->findBy([
            'status' => Receipt::STATUS_WIN,
            'round' => $round,
        ]);
    }

    public function existsWinnerByBkp(string $bkp) : bool
    {
        $count = $this->dao->countBy([
            'status' => Receipt::STATUS_WIN,
            'bkp' => $bkp,
        ]);

        return $count !== 0;
    }

    public function existsReceiptByBkpAndRound(string $bkp, int $round) : bool
    {
        $count = $this->dao->countBy([
            'bkp' => $bkp,
            'round' => $round,
        ]);

        return $count !== 0;
    }
}

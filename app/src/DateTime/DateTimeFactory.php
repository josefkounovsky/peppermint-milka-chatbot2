<?php

declare(strict_types=1);

namespace App\DateTime;

class DateTimeFactory
{
    public function current() : \DateTime
    {
        return new \DateTime();
    }
}
